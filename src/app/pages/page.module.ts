import { NgModule } from '@angular/core'
import { ReactiveFormsModule } from '@angular/forms'
import { PageRoutingModule } from './page-routing.module'
import { RouterModule } from '@angular/router'
import { HttpModule } from '@angular/http'
import { CommonModule } from '@angular/common'
import { PageMainComponent } from './page-main.component'
import { HttpService } from '../http.service'
import { AuthGuard } from './pages-guard.provider'
import { NavComponent } from './navbar/nav.component'
import { ContainerComponent } from './container.component'
import { ControlModule } from '../control/control.module'
import { Validator } from '../control/validator'

@NgModule({
    imports: [
        ReactiveFormsModule,
        HttpModule,
        PageRoutingModule,
        RouterModule,
        CommonModule,
        ControlModule
    ],
    declarations: [
        PageMainComponent,
        NavComponent,
        ContainerComponent,
    ],
    providers: [
        HttpService,
        AuthGuard,
        Validator
    ],
})
export class PageModule { }
