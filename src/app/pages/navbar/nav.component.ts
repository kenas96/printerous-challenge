import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { LocalUser } from '../admin.model'
import { Alert } from '../../control/util'
import swal from 'sweetalert2'

@Component({
  selector: 'nav-component',
  templateUrl: 'nav.component.html',
  styleUrls: ['nav.component.css']
})

export class NavComponent implements OnInit {
  constructor(
    private router: Router,
    private alert: Alert
  ) { }

  localUser: LocalUser
  initAdminDataCallback: Function

  option = {
    page: 1,
    pageSize: 10,
    criteria: [],
    order: {
      column: 'id', direction: 'DESC'
    }
  }

  ngOnInit() {
    this.initAdminData()
    this.initAdminDataCallback = this.initAdminData.bind(this)
  }

  initAdminData() {
    let user = localStorage.getItem('auth-user')
    this.localUser = new LocalUser()
    let decoded: any = atob(user)
    for (var i = 0; i < decoded.length; i++) {
      if (decoded[i] === ':') {
        this.localUser.id = decoded.substr(0, i)
        this.localUser.name = decoded.substr(i + 1, decoded.length - 1)
        break
      }
    }
  }

  logout() {
    swal({
      title: 'Are you sure want to logout?',
      text: "You'll be automatically signed out!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.value) {
        localStorage.clear()
        this.router.navigate(['/login'])
      }
    })
  }

  changePassword() {
    this.alert.warn_alert("Function have not been confirmed")
  }

}