import { NgModule } from '@angular/core'
import { PageMainComponent } from './page-main.component'
import { RouterModule, Routes } from '@angular/router'
import { AuthGuard } from './pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: PageMainComponent,
        children: [
            {
                path: 'organization',
                loadChildren: 'app/organization/organization.module#OrganizationModule',
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                redirectTo: '/pages',
                pathMatch: 'full',
                canActivate: [AuthGuard]
            }
        ]
    }
]



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class PageRoutingModule { }
