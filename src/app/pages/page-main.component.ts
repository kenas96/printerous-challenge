import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'page-main',
    template: `
    <nav-component></nav-component>
    <router-outlet></router-outlet>
  `
})

export class PageMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}