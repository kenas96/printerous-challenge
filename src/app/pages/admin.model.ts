export class Admin {
    web_user_id: number
    name: string
    email: string
    web_user_type: number
    web_password: string

    constructor(webuserid?, name?, email?, webusertype?) {
        this.web_user_id = webuserid
        this.name = name
        this.email = email
        this.web_user_type = webusertype
    }
}

export class Login {
    username: string
    password: string

    constructor(data: any = {}) {
        this.username = data.username
        this.password = data.password
    }
}

export class LocalUser {
    id: string
    name: string

    constructor(data: any = {}) {
        this.id = data.id
        this.name = data.name
    }
}