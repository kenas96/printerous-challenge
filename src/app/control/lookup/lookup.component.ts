import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'lookup',
    templateUrl: './lookup.component.html',
    styleUrls: ['./lookup.component.css']
})

export class LookupComponent implements OnInit {
    constructor() { }

    ngOnInit() { }

    @Input() data = {
        rows: [],
        rowCount: 0,
        pageCount: 0,
        pageSize: 10
    };
    @Input() columns = [];
    @Input() options = {
        page: 1,
        pageSize: 10,
        criteria: [],
        order: {
            column: '', direction: ''
        }

    };
    @Input() display: Function;
    @Input() select(r) {
    }
    @Input() listCriteriaLookup = [];
    placeholderCriteria: any = 'Type to search...';
    direction: boolean = false;
    criteria: any = '';
    value: string = '';

    displayData() {
        this.display();
    }

    changePage() {
        if (this.options.page <= this.data.pageCount) {
            this.display()
        }
    }

    prev() {
        if (this.options.page > 1) {
            this.options.page--;
            this.display();
        }
    }

    next() {
        if (this.options.page < this.data.pageCount) {
            this.options.page++;
            this.display();
        }
    }

    sort(c) {
        this.options.order.column = c;
        this.options.order.direction = this.direction ? 'ASC' : 'DESC';
        this.direction = !this.direction;
        let sortflag = 1;
        this.display();
    }

    searchLookup() {
        if (this.criteria !== '') {
            this.options.criteria.splice(0, 1);
            this.options.criteria.push({ criteria: this.criteria, value: this.value });
            this.options.page = 1;
            this.display();
        }
        else {
            $(".popuptext").fadeIn(500);
            setTimeout(() => {
                $(".popuptext").fadeOut(2500);
            }, 3000)
        }
    }

    changeCriteria(crt) {
        this.criteria = crt.value
        this.placeholderCriteria = crt.text
        this.value = ''
    }

}