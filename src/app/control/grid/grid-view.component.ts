import { Component, OnInit, Input } from '@angular/core'

@Component({
    selector: 'grid-view',
    templateUrl: './grid-view.component.html',
    styleUrls: ['./grid-view.component.css']
})

export class GridviewComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
    @Input() columns = []
    @Input() listCriteria = []
    @Input() permission = {}
    @Input() data = {
        rows: [],
        rowCount: 0,
        pageCount: 0,
        pageSize: 10,
    }
    @Input() options = {
        page: 1,
        pageSize: 10,
        criteria: [],
        order: {
            column: '', direction: ''
        }
    }
    @Input() display: Function

    @Input() view(obj) {
    }
    @Input() edit(obj) {
    }
    @Input() remove(obj) {
    }
    placeholderCriteria: any = 'Type to search...';
    direction: boolean = false
    criteria: any = ''
    value: string = ''

    displayData() {
        this.display();
    }

    PageSizeChange() {
        this.options.page = 1
        this.display()
    }

    changePage() {
        if (this.options.page <= this.data.pageCount) {
            this.display()
        }
    }

    prev() {
        if (this.options.page > 1) {
            this.options.page--
            this.display()
        }
    }

    next() {
        if (this.options.page < this.data.pageCount) {
            this.options.page++
            this.display()
        }
    }

    search() {
        if (this.criteria !== '') {
            this.options.criteria.splice(0, 1);
            this.options.criteria.push({ criteria: this.criteria, value: this.value });
            this.options.page = 1;
            this.display()
        }
        else {
            $("#myPopup").fadeIn(500);
            setTimeout(() => {
                $("#myPopup").fadeOut(2500);
            }, 3000)
        }
    }

    sort(c) {
        if (c != 'NoSort') {
            this.options.order.column = c
            this.options.order.direction = this.direction ? 'ASC' : 'DESC'
            this.direction = !this.direction
            this.display()
        }
    }

    changeCriteria(crt) {
        this.criteria = crt.value
        this.placeholderCriteria = crt.text
        this.value = ''
    }

}