import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { GridviewComponent } from './grid/grid-view.component'
import { LookupComponent } from './lookup/lookup.component'
import { FormsModule } from '@angular/forms'
import { Alert } from '../control/util'


@NgModule({
    imports: [
        FormsModule,
        CommonModule
    ],
    exports: [
        GridviewComponent,
        LookupComponent,

    ],
    declarations: [
        GridviewComponent,
        LookupComponent,

    ],
    providers: [
        Alert,
    ],
})
export class ControlModule { }

