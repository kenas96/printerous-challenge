import swal from 'sweetalert2'

export class Alert {

    loading_login_start() {
        swal({
            title: 'Sign in processing...',
            allowOutsideClick: false,
            allowEscapeKey: false
        })
        swal.showLoading()
    }

    loading_start() {
        swal({
            title: 'Please wait...',
            allowOutsideClick: false,
            allowEscapeKey: false
        })
        swal.showLoading()
    }

    loading_stop() {
        setTimeout(() => {
            swal.close()
        }, 500)
    }

    warning_alert() {
        this.loading_stop()
        setTimeout(() => {
            swal({
                type: 'warning',
                title: 'Oops...',
                text: 'Request failed, please check your connection and try again!',
            })
        }, 500)
    }

    error_alert(err_message: string) {
        this.loading_stop()
        setTimeout(() => {
            swal({
                type: 'error',
                title: 'Oops...',
                text: err_message,
            })
        }, 500)
    }

    warn_alert(err_message: string) {
        this.loading_stop()
        setTimeout(() => {
            swal({
                type: 'warning',
                title: 'Oops...',
                text: err_message,
            })
        }, 500)
    }

    success_alert(succ_message: string, id?, display_func?) {
        this.loading_stop()
        setTimeout(() => {
            $('#' + id).modal('hide')
            swal({
                toast: true,
                type: 'success',
                title: succ_message,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
            })
        }, 500)
        // .then((result) => {
        //     // if (result.dismiss === swal.DismissReason.timer) {
        //     //     $('#' + id).modal('hide')
        //     //     display_func
        //     // }
        // })
    }
}


