import { Component, OnInit, Input } from '@angular/core'
// import { ProductService } from '../../product/product.service'
import { Alert } from '../../control/util'

@Component({
    selector: 'lookup-bonusProduct',
    template: `
    <div class="modal-dialog modal-md">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <span class="close" data-dismiss="modal" style=";color:white;">&times;</span>
                Select Bonus Product
            </div>
            <div class="panel-body">
                <lookup 
                [columns]="columns"
                [data]="data" 
                [display]="displayCallback" 
                [select]="selectCallback"
                [options]="option"
                [listCriteriaLookup]="listCriteriaLookup"
                ></lookup>
            </div>
            <div class="panel-footer" style="text-align: right">
                <button class="btn btn-default" (click)="close()"><i class="fa fa-times-circle" style="margin-right: 5px"></i>Close</button>
            </div>
        </div>
    </div>  
    `,
    styles: []
})

export class LookupBonusProductComponent implements OnInit {
    constructor(
        // private service: ProductService,
        private alert: Alert
    ) { }

    //@Input() product
    @Input() select(obj) {
    }
    displayCallback: Function
    selectCallback: Function

    data = {
        rows: [],
        rowCount: 0,
        pageCount: 0,
        pageSize: 10,
    }

    columns = [
        { field: 'ProdCode', text: 'Product Code' },
        { field: 'ProdName', text: 'Product Name' },
        { field: 'ProdSize', text: 'Product Size' },
    ]

    listCriteriaLookup = [
        { text: 'Product Code', value: 'ProdCode' },
        { text: 'Product Name', value: 'ProdName' },
        { text: 'Product Size', value: 'ProdSize' },
    ]

    option = {
        page: 1,
        pageSize: 10,
        criteria: [],
        order: { column: 'ProdCode', direction: 'ASC' }
    }

    ngOnInit() {
        this.displayCallback = this.displayData.bind(this)
        this.selectCallback = this.select.bind(this)
    }

    ngAfterViewInit() {
        this.displayData()
    }

    loadingAnimationGridStart() {
        this.alert.loading_start()
    }

    loadingAnimationGridStop() {
        this.alert.loading_stop()
    }

    displayData() {
        this.loadingAnimationGridStart()
        // this.service.getPagingBonus(this.option).then(
        //     res => {
        //         this.data = res.data
        //         this.loadingAnimationGridStop()
        //     },
        //     err => {
        //         this.alert.warning_alert()
        //     }
        // )
    }

    close() {
        $('#lookup-bonusProduct').modal('hide')
        $('body').addClass('modal-open')
    }
}