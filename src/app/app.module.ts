import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ControlModule } from './control/control.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { HttpService } from './http.service';
import { LoginService } from './auth/login/login.service';
import { AuthGuard } from './pages/pages-guard.provider';
import { Alert } from './control/util';
import { Validator } from './control/validator';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    RouterModule,
    ControlModule
  ],
  providers: [
    HttpService, 
    LoginService,
    AuthGuard, 
    Alert,
    Validator
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
