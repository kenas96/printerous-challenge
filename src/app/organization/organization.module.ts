import { NgModule } from '@angular/core'
import { HttpService } from '../http.service'
import { CommonModule } from '@angular/common'
import { ControlModule } from '../control/control.module'
import { ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { RouterModule } from '@angular/router'
import { OrganizationListComponent } from './list/organization-list.component'
import { OrganizationDetailComponent } from './detail/organization-detail.component'
import { OrganizationService } from './organization.service'
import { PersonService } from './person/person.service'
import { OrganizationRoutingModule } from './organization-routing.module'
import { OrganizationMainComponent } from './organization-main.component'
import { PersonComponent } from './person/person.component'
import { AuthGuard } from '../pages/pages-guard.provider'
import { Validator } from '../control/validator'
import { Alert } from '../control/util'

@NgModule({
    imports: [
        CommonModule,
        ControlModule,
        ReactiveFormsModule,
        HttpModule,
        RouterModule,
        OrganizationRoutingModule],
    exports: [],
    declarations: [
        OrganizationListComponent,
        OrganizationDetailComponent,
        OrganizationMainComponent,
        PersonComponent
    ],
    providers: [
        OrganizationService,
        PersonService,
        HttpService,
        AuthGuard,
        Validator,
        Alert
    ],
})
export class OrganizationModule { }
