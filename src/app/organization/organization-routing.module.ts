import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { OrganizationMainComponent } from './organization-main.component'
import { OrganizationListComponent } from './list/organization-list.component'
import { AuthGuard } from '../pages/pages-guard.provider'


const routes: Routes = [
    {
        path: '',
        component: OrganizationMainComponent,
        children: [
            {
                path: '',
                component: OrganizationListComponent,
                canActivate: [AuthGuard]
            },
            {
                path: '**',
                component: OrganizationListComponent,
                canActivate: [AuthGuard]
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [],
    declarations: [],
    providers: [],
})
export class OrganizationRoutingModule { }
