import { Component, OnInit, Input, ViewChild } from '@angular/core'
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms'
import { Organization } from './../organization.model'
import { Person } from './../person/person.model'
import { OrganizationService } from './../organization.service'
import { PersonService } from './../person/person.service'
import { Alert } from '../../control/util'
import { Validator } from '../../control/validator'
import 'rxjs/add/operator/debounceTime'
import swal from 'sweetalert2'

@Component({
    selector: 'organization-detail',
    templateUrl: './organization-detail.component.html',
    styleUrls: ['./organization-detail.component.css']
})

export class OrganizationDetailComponent implements OnInit {
    constructor(
        private personService: PersonService,
        private service: OrganizationService,
        private fb: FormBuilder,
        private validator: Validator,
        private alert: Alert
    ) { }

    @Input() id: string
    @Input() organization: Organization
    @Input() state: number
    @Input() displayData: Function
    @Input() organizationForm: FormGroup
    @ViewChild('uploadimage') childImage
    displayCallback: Function
    person: Person
    personForm: FormGroup
    tempTypeImage: string
    stateDetail: number
    nameMessage: string
    emailMessage: string
    phoneMessage: string

    private validationMessage = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 5 characters.",
        maxlength: "Max. character allowed are 50 characters.",
        string: "Please enter a valid character format.",
        pattern: "Please enter a valid email address."
    }

    private validationMessageNumber = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 5 characters.",
        maxlength: "Max. character allowed are 20 characters.",
        number: "Please enter a valid number format.",
    }


    ngOnInit() {
        this.person = new Person()
        this.displayCallback = this.displayData.bind(this)
        const nameControl = this.organizationForm.get('name')
        nameControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageName(nameControl)
        )

        const emailControl = this.organizationForm.get('email')
        emailControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageEmail(emailControl)
        )

        const phoneControl = this.organizationForm.get('phone')
        phoneControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessagePhone(phoneControl)
        )

        const websiteControl = this.organizationForm.get('website')
        websiteControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageWebsite(websiteControl)
        )

        this.personForm = this.fb.group({
            id: '',
            name: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
            email: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]],
            phone: ['', [Validators.required, this.validator.isNumber, Validators.minLength(5), Validators.maxLength(20)]],
            avatar: ['', [Validators.required]],
        })
    }

    setMessageName(c: AbstractControl): void {
        this.nameMessage = ''
        if (this.state == 0) {
            this.organizationForm.get('name').disable()
        } else {
            this.organizationForm.get('name').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.nameMessage = Object.keys(c.errors).map(key =>
                    this.validationMessage[key]
                )[0]
            }
        }
    }

    setMessageEmail(c: AbstractControl): void {
        this.emailMessage = ''
        if (this.state == 0) {
            this.organizationForm.get('email').disable()
        } else {
            this.organizationForm.get('email').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.emailMessage = Object.keys(c.errors).map(key =>
                    this.validationMessage[key]
                )[0]
            }
        }
    }

    setMessagePhone(c: AbstractControl): void {
        this.phoneMessage = ''
        if (this.state == 0) {
            this.organizationForm.get('phone').disable()
        } else {
            this.organizationForm.get('phone').enable()
            if ((c.touched || c.dirty) && c.errors) {
                this.phoneMessage = Object.keys(c.errors).map(key =>
                    this.validationMessageNumber[key]
                )[0]
            }
        }
    }

    setMessageWebsite(c: AbstractControl): void {
        if (this.state == 0) {
            this.organizationForm.get('website').disable()
        } else {
            this.organizationForm.get('website').enable()
        }
    }

    uploadHandler(image) {
        var files = image.target.files;
        var file = files[0];
        var type;
        if (files && file) {
            var reader = new FileReader();
            let imgType = file.type;
            for (var i = 0; i < imgType.length; i++) {
                if (imgType[i] === '/') {
                    type = imgType.substr(i + 1, imgType.length);
                    break;
                }
            }
        }
        if (type !== "jpeg" && type !== "png" && type !== "gif") {
            this.childImage.nativeElement.value = "";
            alert("File Type Must JPEG/JPG/PNG/GIF");
        } else {
            this.organization.logoName = file.name;
            this.organizationForm.patchValue({
                logoName: this.organization.logoName,
            })
            this.tempTypeImage = file.type;
            reader.onload = this.handleReaderLoaded.bind(this);
            reader.readAsBinaryString(file);
        }
    }

    handleReaderLoaded(readerImg) {
        var binaryString = readerImg.target.result;
        this.organization.logo = "data:" + this.tempTypeImage + ";base64," + btoa(binaryString);
        this.organizationForm.patchValue({
            logo: this.organization.logo,
        })
    }

    populateData() {
        this.personForm.patchValue({
            id: this.person.id,
            name: this.person.name,
            email: this.person.email,
            phone: this.person.phone,
            avatar: this.person.avatar
        })
    }

    focusField() {
        setTimeout(() => {
            $("#name_person").focus()
        }, 500)
    }

    addNewPerson() {
        this.stateDetail = 1
        this.person = new Person()
        this.personForm.reset()
        this.focusField()
        $("#uploadimagePerson").val('')
        $('#modal-person').modal()
    }

    editPerson(obj) {
        this.stateDetail = -1
        this.person = JSON.parse(JSON.stringify(obj))
        this.personForm.reset()
        this.focusField()
        this.populateData()
        $("#uploadimagePerson").val('')
        $('#modal-person').modal()
    }

    deletePerson(obj) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                this.alert.loading_start()
                this.personService.delete(obj.id).then(
                    res => {
                        this.alert.success_alert('Your data has been removed!', this.id, this.displayData())
                    },
                    err => {
                        if (err.http_status === 422) {
                            this.alert.error_alert(err.message)
                        } else {
                            this.alert.warn_alert(err.message)
                        }
                    }
                )
            }
        })
    }

    save() {
        this.alert.loading_start()
        let obj = Object.assign({}, this.organization, this.organizationForm.value)

        if (this.state == 1) {
            this.service.insert(obj).then(
                res => {
                    this.alert.success_alert('Your data has been added', this.id, this.displayData())
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
        } else {
            obj.persons = undefined
            obj.account_permission = undefined
            this.service.update(obj).then(
                res => {
                    this.alert.success_alert('Your data has been saved', this.id, this.displayData())
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
        }
    }
}
