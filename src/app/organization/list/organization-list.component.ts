import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { OrganizationService } from '../organization.service'
import { Organization } from '../organization.model'
import { JwtHelper } from 'angular2-jwt'
import { Validator } from '../../control/validator'
import { Alert } from '../../control/util'
import swal from 'sweetalert2'

@Component({
    selector: 'organization-list',
    templateUrl: './organization-list.component.html',
    styleUrls: ['./organization-list.component.css']
})

export class OrganizationListComponent implements OnInit {
    constructor(
        private service: OrganizationService,
        private fb: FormBuilder,
        private validator: Validator,
        private alert: Alert
    ) { }

    state: number = 1
    organization: Organization
    organizationForm: FormGroup
    displayCallback: Function
    viewCallback: Function
    editCallback: Function
    deleteCallback: Function
    jwtHelper: JwtHelper = new JwtHelper()

    data = {
        rows: [],
        rowCount: 0,
        pageCount: 0,
        pageSize: 10,
    }

    option = {
        page: 1,
        pageSize: 10,
        criteria: [],
        order: {
            column: 'id', direction: 'ASC'
        }
    }

    columns = [
        { text: 'Name', field: 'name', value: 'name' },
        { text: 'Phone', field: 'phone', value: 'phone' },
        { text: 'Email', field: 'email', value: 'email' },
        { text: 'Website', field: 'website', value: 'website' },
    ]

    listCriteria = [
        { text: 'Name', value: 'name' },
        { text: 'Phone', value: 'phone' },
        { text: 'email', value: 'email' },
    ]

    permission: any = {
        create: true,
        view: true,
        update: true,
        delete: true
    }


    ngOnInit() {
        this.organization = new Organization()
        this.displayCallback = this.displayData.bind(this)
        this.viewCallback = this.view.bind(this)
        this.editCallback = this.edit.bind(this)
        this.deleteCallback = this.remove.bind(this)
        this.organizationForm = this.fb.group({
            id: '',
            name: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(100)]],
            email: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}')]],
            phone: ['', [Validators.required, this.validator.isNumber, Validators.minLength(5), Validators.maxLength(20)]],
            website: '',
            logo: '',
            account_management_id: '',
        })
    }

    ngAfterViewInit() {
        this.displayData()
    }

    loadingAnimationGridStart() {
        document.getElementById('dataGrid').style.display = "none"
        document.getElementById('loadingGrid').style.display = "block"
    }

    loadingAnimationGridStop() {
        document.getElementById('dataGrid').style.display = "block"
        document.getElementById('loadingGrid').style.display = "none"
    }

    parseData() {
        let user = this.jwtHelper.decodeToken(localStorage.getItem('auth-token'))
        for (let entry of this.data.rows) {
            if (entry['account_management_id'] == user.data.id) {
                entry['account_permission'] = true
            } else {
                entry['account_permission'] = false
            }
        }
    }

    displayData() {
        this.loadingAnimationGridStart()
        this.service.getPaging(this.option).
            then(
                res => {
                    this.data = res.data;
                    this.parseData()
                    this.loadingAnimationGridStop()
                },
                err => {
                    this.alert.warning_alert()
                    this.loadingAnimationGridStop()
                }
            )
    }

    focusField() {
        setTimeout(() => {
            $("#name").focus()
        }, 500)
    }

    populateData() {
        this.organizationForm.patchValue({
            id: this.organization.id,
            name: this.organization.name,
            email: this.organization.email,
            phone: this.organization.phone,
            website: this.organization.website,
            logo: this.organization.logo,
            account_management_id: this.organization.account_management_id,
        })
    }

    addNew() {
        this.state = 1
        this.organization = new Organization()
        this.organizationForm.reset()
        this.focusField()
        $("#uploadimage").val('')
        $('#modal-detail').modal()
    }

    view(obj) {
        this.state = 0
        this.organization = obj
        this.organizationForm.reset()
        this.populateData()
        $("#uploadimage").val('')
        $('#modal-detail').modal()
    }

    edit(obj) {
        this.state = -1
        this.organization = JSON.parse(JSON.stringify(obj))
        this.organizationForm.reset()
        this.focusField()
        this.populateData()
        $("#uploadimage").val('')
        $('#modal-detail').modal()
    }

    remove(obj) {
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                this.alert.loading_start()
                this.service.delete(obj.id).then(
                    res => {
                        this.alert.success_alert('Your data has been removed!', '', this.displayData())
                    },
                    err => {
                        if (err.http_status === 422) {
                            this.alert.error_alert(err.message)
                        } else {
                            this.alert.warn_alert(err.message)
                        }
                    }
                )
            }
        })
    }
}