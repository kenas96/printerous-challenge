import { Injectable } from '@angular/core'
import { HttpService } from '../../http.service'

@Injectable()
export class PersonService {
    constructor(private service: HttpService) { }

    insert(data) {
        return this.service.post('persons', data)
    }

    update(data) {
        return this.service.put('persons', data)
    }

    delete(id) {
        return this.service.delete('persons/' + id)
    }
}