import { Component, OnInit, Input, ViewChild } from '@angular/core'
import { FormGroup, FormBuilder, AbstractControl } from '@angular/forms'
import { PersonService } from './person.service'
import { Person } from './person.model';
import { Alert } from '../../control/util'
import { Organization } from './../organization.model';
import 'rxjs/add/operator/debounceTime'

@Component({
    selector: 'person-detail',
    templateUrl: './person.component.html',
    styles: []
})

export class PersonComponent implements OnInit {
    constructor(
        private fb: FormBuilder,
        private alert: Alert,
        private service: PersonService
    ) { }

    @Input() organization: Organization
    @Input() person: Person
    @Input() stateDetail: number
    @Input() personForm: FormGroup
    @Input() displayData: Function
    @ViewChild('uploadimagePerson') childImage

    tempTypeImage: string
    nameMessage: string
    emailMessage: string
    phoneMessage: string

    private validationMessage = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 5 characters.",
        maxlength: "Max. character allowed are 50 characters.",
        string: "Please enter a valid character format.",
        pattern: "Please enter a valid email address."
    }

    private validationMessageNumber = {
        required: "Please fill this field.",
        minlength: "Min. character allowed are 5 characters.",
        maxlength: "Max. character allowed are 20 characters.",
        number: "Please enter a valid number format.",
    }

    ngOnInit() {
        const nameControl = this.personForm.get('name')
        nameControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageName(nameControl)
        )

        const emailControl = this.personForm.get('email')
        emailControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageEmail(emailControl)
        )

        const phoneControl = this.personForm.get('phone')
        phoneControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessagePhone(phoneControl)
        )
    }

    setMessageName(c: AbstractControl): void {
        this.nameMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.nameMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }
    }

    setMessageEmail(c: AbstractControl): void {
        this.emailMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.emailMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }
    }


    setMessagePhone(c: AbstractControl): void {
        this.phoneMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.phoneMessage = Object.keys(c.errors).map(key =>
                this.validationMessageNumber[key]
            )[0]
        }
    }

    uploadHandler(image) {
        var files = image.target.files;
        var file = files[0];
        var type;
        if (files && file) {
            var reader = new FileReader();
            let imgType = file.type;
            for (var i = 0; i < imgType.length; i++) {
                if (imgType[i] === '/') {
                    type = imgType.substr(i + 1, imgType.length);
                    break;
                }
            }
        }
        if (type !== "jpeg" && type !== "png" && type !== "gif") {
            this.childImage.nativeElement.value = "";
            alert("File Type Must JPEG/JPG/PNG/GIF");
        } else {
            this.person.avatarName = file.name;
            this.personForm.patchValue({
                avatarName: this.person.avatarName,
            })
            this.tempTypeImage = file.type;
            reader.onload = this.handleReaderLoaded.bind(this);
            reader.readAsBinaryString(file);
        }
    }

    handleReaderLoaded(readerImg) {
        var binaryString = readerImg.target.result;
        this.person.avatar = "data:" + this.tempTypeImage + ";base64," + btoa(binaryString);
        this.personForm.patchValue({
            avatar: this.person.avatar,
        })
    }

    save() {
        this.alert.loading_start()
        let obj = Object.assign({}, this.person, this.personForm.value)
        if (this.stateDetail == 1) {
            obj.organization_id = this.organization.id
            this.service.insert(obj).then(
                res => {
                    this.alert.success_alert('Your data has been added', 'modal-person', this.displayData())
                    $('#modal-detail').modal('hide')
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
        } else {
            this.service.update(obj).then(
                res => {
                    this.alert.success_alert('Your data has been saved', 'modal-person', this.displayData())
                    $('#modal-detail').modal('hide')
                },
                err => {
                    if (err.http_status === 422) {
                        this.alert.error_alert(err.message)
                    } else {
                        this.alert.warn_alert(err.message)
                    }
                }
            )
        }

    }
}