export class Person {
    id: number
    name: string
    email: string
    phone: string
    avatar: string
    avatarName: string

    constructor(data: any = {}) {
        this.id = data.id
        this.name = data.name
        this.email = data.email
        this.phone = data.phone
        this.avatar = data.avatar
        this.avatarName = data.avatarName
    }
}