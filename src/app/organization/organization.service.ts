import { Injectable } from '@angular/core'
import { HttpService } from '../http.service'

@Injectable()
export class OrganizationService {

    constructor(private service: HttpService) { }


    getPaging(data) {
        return this.service.post('organizations/paging', data)
    }

    insert(data) {
        return this.service.post('organizations', data)
    }

    update(data) {
        return this.service.put('organizations', data)
    }

    delete(id) {
        return this.service.delete('organizations/' + id)
    }
}