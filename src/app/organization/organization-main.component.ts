import { Component, OnInit } from '@angular/core'

@Component({
    selector: 'main-organization',
    template: '<router-outlet></router-outlet>'
})

export class OrganizationMainComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}