import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { LoginService } from './login.service'
import { Admin, Login } from '../../pages/admin.model'
import { JwtHelper } from 'angular2-jwt'
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms'
import { Alert } from '../../control/util'
import 'rxjs/add/operator/debounceTime'

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
    constructor(
        private router: Router,
        private service: LoginService,
        private fb: FormBuilder,
        private alert: Alert
    ) { }

    admin: Admin
    loginObj: Login
    initAdminDataCallback: Function
    jwtHelper: JwtHelper = new JwtHelper()
    loginForm: FormGroup
    emailMessage: string
    passwordMessage: string

    private validationMessage = {
        required: "Please fill this field."
    }

    ngOnInit() {
        this.initAdminData()
        this.initAdminDataCallback = this.initAdminData.bind(this)
        this.loginForm = this.fb.group({
            email: ['', [Validators.required]],
            password: ['', [Validators.required]]
        })

        const emailControl = this.loginForm.get('email')
        emailControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessageEmail(emailControl)
        )

        const passwordControl = this.loginForm.get('password')
        passwordControl.valueChanges.debounceTime(100).subscribe(value =>
            this.setMessagePassword(passwordControl)
        )

        setTimeout(() => {
            $("#email").focus()
        }, 500)
    }

    setMessageEmail(c: AbstractControl): void {
        this.emailMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.emailMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }
    }

    setMessagePassword(c: AbstractControl): void {
        this.passwordMessage = ''
        if ((c.touched || c.dirty) && c.errors) {
            this.passwordMessage = Object.keys(c.errors).map(key =>
                this.validationMessage[key]
            )[0]
        }
    }

    initAdminData() {
        this.admin = new Admin()
        this.loginObj = new Login()
    }

    focusField() {
        setTimeout(() => {
            $("#email").focus()
        }, 500)
    }

    forgot() {
        this.alert.warn_alert("Function have not been confirmed")
    }

    login() {
        let obj = Object.assign({}, this.loginObj, this.loginForm.value)
        this.alert.loading_login_start()
        this.service.login(obj).then(
            res => {
                let varBase64 = res.data.user.id + ":" + res.data.user.name
                let data = btoa(varBase64)

                this.alert.success_alert('Successfully signed', '', '')
                localStorage.setItem('auth-token', res.data.token)
                localStorage.setItem('auth-user', data)
                this.router.navigate(['/pages/organization'])
            },
            err => {
                if (err.http_status === 422) {
                    this.alert.error_alert(err.message)
                } else {
                    this.alert.warn_alert(err.message)
                }
            }
        )
    }
}