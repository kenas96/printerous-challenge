import { Injectable } from '@angular/core'
import { HttpService } from '../../http.service'

@Injectable()
export class LoginService {

    constructor(private service: HttpService) { }

    login(data) {
        return this.service.postLogin('login', data)
    }

}